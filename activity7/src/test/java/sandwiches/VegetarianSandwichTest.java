package sandwiches;

import static org.junit.Assert.*; 
import org.junit.Test;

public class VegetarianSandwichTest {
    
    @Test 
    public void constructorTest(){
        VegetarianSandwich vs = new TofuSandwich();

        assertEquals("tofu", vs.getFilling());
    }

    @Test
    public void addFillingTestSucceed(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("lettuce");
        assertEquals("tofu, lettuce", vs.getFilling());

        vs.addFilling("tomato");
        assertEquals("tofu, lettuce, tomato", vs.getFilling());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFillingTestChicken(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("chicken");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFillingTestBeef(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("beef");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFillingTestFish(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("fish");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFillingTestMeat(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("meat");

    }

    @Test(expected = IllegalArgumentException.class)
    public void addFillingTestPork(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("pork");
    }

    @Test
    public void isVegetarianAlwaysTrue(){
        VegetarianSandwich vs = new TofuSandwich();

        assertEquals(true, vs.isVegetarian());
    }

    @Test
    public void isVeganTrueTest(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("lettuce");
        assertEquals(true, vs.isVegan());
    }

    @Test
    public void isVeganEggFalseTest(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("egg");
        assertEquals(false, vs.isVegan());
    }

    @Test
    public void isVeganCheeseFalseTest(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("cheese");
        assertEquals(false, vs.isVegan());
    }

    @Test
    public void isVeganMultipleToppingFalseTest(){
        VegetarianSandwich vs = new TofuSandwich();

        vs.addFilling("tomato");
        vs.addFilling("cheese");
        assertEquals(false, vs.isVegan());
    }
}