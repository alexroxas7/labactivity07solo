package sandwiches;

public class TofuSandwich extends VegetarianSandwich{
    
    public TofuSandwich(){
        this.addFilling("tofu");
    }

    @Override
    public String getProtein(){
        return "tofu";
    }
}
