package sandwiches;
import java.util.*;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ISandwich ssi = new TofuSandwich();

        if (ssi instanceof VegetarianSandwich){
            System.out.println("Veggyboy");
        } else if (ssi instanceof ISandwich){
            System.out.println("Interfaceboy");
        }
        VegetarianSandwich casted = (VegetarianSandwich) ssi;
        casted.isVegan();
    }
}
